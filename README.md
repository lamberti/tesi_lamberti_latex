# tesi_lamberti_LaTeX

In order to compile the Feynman diagrams, clone the [Tikz-Feynman](https://github.com/JP-Ellis/tikz-feynman) git repository.

To compile the PDF file use the `Makefile` running the command

    make

It will produce the file `principale.pdf`.